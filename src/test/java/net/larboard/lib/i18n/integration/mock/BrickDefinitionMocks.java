package net.larboard.lib.i18n.integration.mock;

import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import net.larboard.lib.i18n.enumeration.BrickType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BrickDefinitionMocks {
    private BrickDefinitionMocks() {}
    
    public static final BrickDefinitionRest NEW_VALID_DEFINITION = new BrickDefinitionRest();
    static {
        NEW_VALID_DEFINITION.setTemplate(1L);
        NEW_VALID_DEFINITION.setKey("multiline.title");
        NEW_VALID_DEFINITION.setType(BrickType.MARKDOWN);
        NEW_VALID_DEFINITION.setMultiline(true);
        NEW_VALID_DEFINITION.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        NEW_VALID_DEFINITION.setTranslationHint("some text");
        NEW_VALID_DEFINITION.setDeleted(false);
    }

    public static final List<BrickDefinitionRest> NEW_INVALID_DEFINITIONS = new ArrayList<>();
    static {
        BrickDefinitionRest d = new BrickDefinitionRest();
        d.setTemplate(-99L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(true);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey(null);
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(true);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType("SEÖPL");
        d.setMultiline(true);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(null);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(false);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "x")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(false);
        d.setPlaceholders(new ArrayList<>(Arrays.asList(" ")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(true);
        d.setPlaceholders(null);
        d.setTranslationHint("some text");
        d.setDeleted(false);
        NEW_INVALID_DEFINITIONS.add(d);
    }

    public static final BrickDefinitionRest NEW_INVALID_TEMPLATE_DEFINITION = new BrickDefinitionRest();
    static {
        NEW_INVALID_TEMPLATE_DEFINITION.setTemplate(Long.MAX_VALUE);
        NEW_INVALID_TEMPLATE_DEFINITION.setKey("multiline.title");
        NEW_INVALID_TEMPLATE_DEFINITION.setType(BrickType.MARKDOWN);
        NEW_INVALID_TEMPLATE_DEFINITION.setMultiline(true);
        NEW_INVALID_TEMPLATE_DEFINITION.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        NEW_INVALID_TEMPLATE_DEFINITION.setTranslationHint("some text");
        NEW_INVALID_TEMPLATE_DEFINITION.setDeleted(false);
    }

    public static final BrickDefinitionRest NEW_INVALID_CONFLICTING_DEFINITION = new BrickDefinitionRest();
    static {
        NEW_INVALID_CONFLICTING_DEFINITION.setTemplate(1L);
        NEW_INVALID_CONFLICTING_DEFINITION.setKey("multiline.title.egon");
        NEW_INVALID_CONFLICTING_DEFINITION.setType(BrickType.MARKDOWN);
        NEW_INVALID_CONFLICTING_DEFINITION.setMultiline(true);
        NEW_INVALID_CONFLICTING_DEFINITION.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        NEW_INVALID_CONFLICTING_DEFINITION.setTranslationHint("some text");
        NEW_INVALID_CONFLICTING_DEFINITION.setDeleted(false);
    }

    public static final BrickDefinitionRest UPDATE_VALID_DEFINITION = new BrickDefinitionRest();
    static {
        UPDATE_VALID_DEFINITION.setTemplate(1L);
        UPDATE_VALID_DEFINITION.setKey("multiline.title");
        UPDATE_VALID_DEFINITION.setType(BrickType.PLAIN);
        UPDATE_VALID_DEFINITION.setMultiline(false);
        UPDATE_VALID_DEFINITION.setPlaceholders(new ArrayList<>(Arrays.asList("a", "b")));
        UPDATE_VALID_DEFINITION.setTranslationHint("some other text");
        UPDATE_VALID_DEFINITION.setDeleted(false);
    }

    public static final List<BrickDefinitionRest> UPDATE_INVALID_DEFINITIONS = new ArrayList<>();
    static {
        BrickDefinitionRest d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType("SEÖPL");
        d.setMultiline(true);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        UPDATE_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(null);
        d.setPlaceholders(new ArrayList<>(Arrays.asList("x", "y")));
        d.setTranslationHint("some text");
        d.setDeleted(false);
        UPDATE_INVALID_DEFINITIONS.add(d);

        d = new BrickDefinitionRest();
        d.setTemplate(1L);
        d.setKey("multiline.title");
        d.setType(BrickType.MARKDOWN);
        d.setMultiline(true);
        d.setPlaceholders(null);
        d.setTranslationHint("some text");
        d.setDeleted(false);
        UPDATE_INVALID_DEFINITIONS.add(d);
    }
}
