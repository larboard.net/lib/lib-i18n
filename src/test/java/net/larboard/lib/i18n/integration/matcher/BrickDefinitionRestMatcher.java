package net.larboard.lib.i18n.integration.matcher;

import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class BrickDefinitionRestMatcher {
    private BrickDefinitionRestMatcher() {}

    public static void expectToBeLike(ResultActions whatIs, BrickDefinitionRest sample) throws Exception {
        expectToBeLike(whatIs, sample, "$");
    }

    public static void expectToBeLike(ResultActions whatIs, BrickDefinitionRest sample, String path) throws Exception {
        whatIs
                .andExpect(jsonPath(path+".template").value(sample.getTemplate()))
                .andExpect(jsonPath(path+".key").value(sample.getKey()))
                .andExpect(jsonPath(path+".type").value(sample.getType()))
                .andExpect(jsonPath(path+".multiline").value(sample.getMultiline()))
                .andExpect(jsonPath(path+".translationHint").value(sample.getTranslationHint()))
                .andExpect(jsonPath(path+".placeholders.length()").value(sample.getPlaceholders().size()));
        int i = 0;
        for(String placeholder : sample.getPlaceholders()) {
            whatIs.andExpect(jsonPath(path+".placeholders["+i+"]").value(placeholder));
            i++;
        }
        whatIs.andExpect(jsonPath(path+".deleted").value(sample.getDeleted()));
    }
}
