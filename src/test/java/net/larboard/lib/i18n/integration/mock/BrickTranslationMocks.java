package net.larboard.lib.i18n.integration.mock;

import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import net.larboard.lib.i18n.data.api.BrickTranslationRest;
import net.larboard.lib.i18n.enumeration.BrickType;

import java.util.ArrayList;

public class BrickTranslationMocks {
    private BrickTranslationMocks() {}

    public static final BrickDefinitionRest DEF_A = new BrickDefinitionRest();
    static {
        DEF_A.setTemplate(1L);
        DEF_A.setKey("brick.test");
        DEF_A.setType(BrickType.MARKDOWN);
        DEF_A.setMultiline(true);
        DEF_A.setPlaceholders(new ArrayList<>());
        DEF_A.setTranslationHint(null);
        DEF_A.setDeleted(false);
    }

    public static final BrickTranslationRest VALID_A = new BrickTranslationRest();
    static {
        VALID_A.setTemplate(1L);
        VALID_A.setKey("brick.test");
        VALID_A.setLang("en");
        VALID_A.setValue("text");
    }

    public static final BrickTranslationRest UPDATE_VALID_A = new BrickTranslationRest();
    static {
        UPDATE_VALID_A.setTemplate(1L);
        UPDATE_VALID_A.setKey("brick.test");
        UPDATE_VALID_A.setLang("en");
        UPDATE_VALID_A.setValue("new text");
    }

    public static final BrickTranslationRest INVALID_A = new BrickTranslationRest();
    static {
        INVALID_A.setTemplate(1L);
        INVALID_A.setKey("not.exist");
        INVALID_A.setLang("en");
        INVALID_A.setValue("text");
    }

    public static final BrickTranslationRest INVALID_B = new BrickTranslationRest();
    static {
        INVALID_B.setTemplate(1L);
        INVALID_B.setKey("brick.test");
        INVALID_B.setLang("it");
        INVALID_B.setValue("text");
    }

    public static final BrickTranslationRest INVALID_C = new BrickTranslationRest();
    static {
        INVALID_C.setTemplate(Long.MAX_VALUE);
        INVALID_C.setKey("brick.test");
        INVALID_C.setLang("en");
        INVALID_C.setValue("text");
    }

    public static final BrickTranslationRest INVALID_D = new BrickTranslationRest();
    static {
        INVALID_D.setTemplate(-99L);
        INVALID_D.setKey("brick.test");
        INVALID_D.setLang("en");
        INVALID_D.setValue("text");
    }
}
