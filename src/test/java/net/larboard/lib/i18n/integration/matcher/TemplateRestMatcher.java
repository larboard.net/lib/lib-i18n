package net.larboard.lib.i18n.integration.matcher;

import net.larboard.lib.i18n.data.api.TemplateRest;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class TemplateRestMatcher {
    private TemplateRestMatcher() {}

    public static void expectToBeLike(ResultActions whatIs, TemplateRest sample) throws Exception {
        expectToBeLike(whatIs, sample, "$");
    }

    public static void expectToBeLike(ResultActions whatIs, TemplateRest sample, String path) throws Exception {
        whatIs
                .andExpect(jsonPath(path+".id").isNotEmpty())
                .andExpect(jsonPath(path+".name").value(sample.getName()))
                .andExpect(jsonPath(path+".slug").value(sample.getSlug()))
                .andExpect(jsonPath(path+".type").value(sample.getType()))
                .andExpect(jsonPath(path+".defaultLanguage").value(sample.getDefaultLanguage()))
                .andExpect(jsonPath(path+".enabledLanguages.length()").value(sample.getEnabledLanguages().size()));
        int i = 0;
        for(String lang : sample.getEnabledLanguages()) {
            whatIs.andExpect(jsonPath(path+".enabledLanguages["+i+"]").value(lang));
            i++;
        }
        whatIs.andExpect(jsonPath(path+".deleted").value(sample.getDeleted()));
    }
}
