package net.larboard.lib.i18n.integration.matcher;

import net.larboard.lib.i18n.data.api.BrickTranslationRest;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class BrickTranslationRestMatcher {
    private BrickTranslationRestMatcher() {}

    public static void expectToBeLike(ResultActions whatIs, BrickTranslationRest sample) throws Exception {
        expectToBeLike(whatIs, sample, "$");
    }

    public static void expectToBeLike(ResultActions whatIs, BrickTranslationRest sample, String path) throws Exception {
        whatIs
                .andExpect(jsonPath(path+".template").value(sample.getTemplate()))
                .andExpect(jsonPath(path+".key").value(sample.getKey()))
                .andExpect(jsonPath(path+".lang").value(sample.getLang()))
                .andExpect(jsonPath(path+".value").value(sample.getValue()));
    }
}
