package net.larboard.lib.i18n.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.DEF_A;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.VALID_A;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class DistIntegrationTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Test
    @Transactional
    public void brickShowsOnNextAndNotOnDist() throws Exception {
        _insertBrick();
        _insertTranslation();

        mvc.perform(MockMvcRequestBuilders.get("/i18n/public/next/default/en")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.brick.test").value("text"));

        mvc.perform(MockMvcRequestBuilders.get("/i18n/public/dist/default/en")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.brick.test").doesNotExist());
    }

    @Test
    @Transactional
    public void brickShowsOnDistAfterRelease() throws Exception {
        _insertBrick();
        _insertTranslation();

        mvc.perform(MockMvcRequestBuilders.get("/i18n/public/dist/default/en")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.brick.test").doesNotExist());

        mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/template/1/release"));

        mvc.perform(MockMvcRequestBuilders.get("/i18n/public/dist/default/en")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.brick.test").value("text"));
    }

    private void _insertBrick() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(DEF_A)))
                .andExpect(status().isOk());
    }

    private void _insertTranslation() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk());
    }
}
