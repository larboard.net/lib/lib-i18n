package net.larboard.lib.i18n.integration.mock;

import net.larboard.lib.i18n.data.api.TemplateRest;
import net.larboard.lib.i18n.enumeration.TemplateType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TemplateMocks {
    private TemplateMocks() {}

    public static final TemplateRest DEFAULT_TEMPLATE = new TemplateRest();
    static {
        DEFAULT_TEMPLATE.setId(1L);
        DEFAULT_TEMPLATE.setName("default");
        DEFAULT_TEMPLATE.setSlug("default");
        DEFAULT_TEMPLATE.setType(TemplateType.SITE);
        DEFAULT_TEMPLATE.setDefaultLanguage("en");
        DEFAULT_TEMPLATE.setEnabledLanguages(new ArrayList<>(Arrays.asList("en")));
        DEFAULT_TEMPLATE.setDeleted(false);
    }

    public static final TemplateRest NEW_VALID_TEMPLATE = new TemplateRest();
    static {
        NEW_VALID_TEMPLATE.setId(null);
        NEW_VALID_TEMPLATE.setName("New Template");
        NEW_VALID_TEMPLATE.setSlug("supertemplate");
        NEW_VALID_TEMPLATE.setType(TemplateType.PDF);
        NEW_VALID_TEMPLATE.setDefaultLanguage("fr");
        NEW_VALID_TEMPLATE.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        NEW_VALID_TEMPLATE.setDeleted(false);
    }

    public static final TemplateRest NEW_DUPLICATE_TEMPLATE = new TemplateRest();
    static {
        NEW_DUPLICATE_TEMPLATE.setId(null);
        NEW_DUPLICATE_TEMPLATE.setName("New Template");
        NEW_DUPLICATE_TEMPLATE.setSlug("default");
        NEW_DUPLICATE_TEMPLATE.setType(TemplateType.PDF);
        NEW_DUPLICATE_TEMPLATE.setDefaultLanguage("fr");
        NEW_DUPLICATE_TEMPLATE.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        NEW_DUPLICATE_TEMPLATE.setDeleted(false);
    }

    public static final List<TemplateRest> NEW_INVALID_TEMPLATES = new ArrayList<>();
    static {
        TemplateRest t = new TemplateRest();
        t.setId(null);
        t.setName(null);
        t.setSlug("supertemplate");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        t.setDeleted(false);
        NEW_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("New Template");
        t.setSlug(null);
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        t.setDeleted(false);
        NEW_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("New Template");
        t.setSlug("supertemplate");
        t.setType("YOLO");
        t.setDefaultLanguage("");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        t.setDeleted(false);
        NEW_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("New Template");
        t.setSlug("supertemplate");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es")));
        t.setDeleted(false);
        NEW_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("New Template");
        t.setSlug("supertemplate");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList()));
        t.setDeleted(false);
        NEW_INVALID_TEMPLATES.add(t);
    }

    public static final TemplateRest UPDATE_VALID_TEMPLATE = new TemplateRest();
    static {
        UPDATE_VALID_TEMPLATE.setId(null);
        UPDATE_VALID_TEMPLATE.setName("Another Template");
        UPDATE_VALID_TEMPLATE.setSlug("default");
        UPDATE_VALID_TEMPLATE.setType(TemplateType.PDF);
        UPDATE_VALID_TEMPLATE.setDefaultLanguage("fr");
        UPDATE_VALID_TEMPLATE.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        UPDATE_VALID_TEMPLATE.setDeleted(false);
    }

    public static final List<TemplateRest> UPDATE_INVALID_TEMPLATES = new ArrayList<>();
    static {
        TemplateRest t = new TemplateRest();
        t.setId(null);
        t.setName(null);
        t.setSlug("oddtpl");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        t.setDeleted(false);
        UPDATE_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("Another Template");
        t.setSlug("oddtpl");
        t.setType("SEPPL");
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es", "fr")));
        t.setDeleted(false);
        UPDATE_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("Another Template");
        t.setSlug("oddtpl");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList("es")));
        t.setDeleted(false);
        UPDATE_INVALID_TEMPLATES.add(t);

        t = new TemplateRest();
        t.setId(null);
        t.setName("Another Template");
        t.setSlug("oddtpl");
        t.setType(TemplateType.PDF);
        t.setDefaultLanguage("fr");
        t.setEnabledLanguages(new ArrayList<>(Arrays.asList()));
        t.setDeleted(false);
        UPDATE_INVALID_TEMPLATES.add(t);
    }
}
