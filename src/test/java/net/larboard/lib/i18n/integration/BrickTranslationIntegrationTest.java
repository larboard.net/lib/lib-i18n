package net.larboard.lib.i18n.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import net.larboard.lib.i18n.integration.matcher.BrickDefinitionRestMatcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static net.larboard.lib.i18n.integration.matcher.BrickTranslationRestMatcher.expectToBeLike;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.DEF_A;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.INVALID_A;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.INVALID_B;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.INVALID_C;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.INVALID_D;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.UPDATE_VALID_A;
import static net.larboard.lib.i18n.integration.mock.BrickTranslationMocks.VALID_A;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BrickTranslationIntegrationTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Test
    @Transactional
    public void emptyBrickShowsOnList() throws Exception {
        _insertValidBrickA();

        val resultActions = mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/bricktranslation/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A))
                .param("targetLanguage", "en")
                .param("sourceLanguage", "en")
                .param("modifiedOnly", "false")
                .param("emptyOnly", "false"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].templateId").value(1))
                .andExpect(jsonPath("$.content[0].sourceNext").isEmpty())
                .andExpect(jsonPath("$.content[0].sourceDist").isEmpty())
                .andExpect(jsonPath("$.content[0].targetNext").isEmpty())
                .andExpect(jsonPath("$.content[0].targetDist").isEmpty());

        BrickDefinitionRestMatcher.expectToBeLike(resultActions, DEF_A, "$.content[0].definition");
    }

    @Test
    @Transactional
    public void translatedBrickShowsOnList() throws Exception {
        saveValidA();

        val resultActions = mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/bricktranslation/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A))
                .param("targetLanguage", "en")
                .param("sourceLanguage", "en")
                .param("modifiedOnly", "false")
                .param("emptyOnly", "false"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].templateId").value(1))
                .andExpect(jsonPath("$.content[0].sourceDist").isEmpty())
                .andExpect(jsonPath("$.content[0].targetDist").isEmpty());

        BrickDefinitionRestMatcher.expectToBeLike(resultActions, DEF_A, "$.content[0].definition");

        expectToBeLike(resultActions, VALID_A, "$.content[0].sourceNext");
        expectToBeLike(resultActions, VALID_A, "$.content[0].targetNext");
    }

    @Test
    @Transactional
    public void saveValidA() throws Exception {
        _insertValidBrickA();

        val resultActions = mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, VALID_A);
    }

    @Test
    @Transactional
    public void updateValidA() throws Exception {
        saveValidA();

        val resultActions = mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(UPDATE_VALID_A)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, UPDATE_VALID_A);
    }

    @Test
    @Transactional
    public void saveInvalidA() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(INVALID_A)))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void saveInvalidB() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(INVALID_B)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    public void saveInvalidC() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(INVALID_C)))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void saveInvalidD() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch("/i18n/translator/translation")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(INVALID_D)))
                .andExpect(status().isBadRequest());
    }

    private void _insertValidBrickA() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(DEF_A)))
                .andExpect(status().isOk());
    }
}
