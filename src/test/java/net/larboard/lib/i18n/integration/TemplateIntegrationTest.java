package net.larboard.lib.i18n.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import net.larboard.lib.i18n.data.api.TemplateRest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static net.larboard.lib.i18n.integration.matcher.TemplateRestMatcher.expectToBeLike;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.DEFAULT_TEMPLATE;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.NEW_DUPLICATE_TEMPLATE;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.NEW_INVALID_TEMPLATES;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.NEW_VALID_TEMPLATE;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.UPDATE_INVALID_TEMPLATES;
import static net.larboard.lib.i18n.integration.mock.TemplateMocks.UPDATE_VALID_TEMPLATE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TemplateIntegrationTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    // TODO: test filtered list

    /*
     * initial default template
     * add a template
     * add invalid template
     *      - name conflict
     *      - invalid fields
     * update template
     * update invalid template
     *      - name conflict
     *      - invalid fields
     * delete template
     */

    @Test
    @Transactional
    public void freshDatabaseHasDefaultTemplate() throws Exception {

        val resultActions = mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/template")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, DEFAULT_TEMPLATE, "$.content[0]");
    }

    @Test
    @Transactional
    public void getExistingTemplateReturnsTemplate() throws Exception {
        val resultActions = mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/template/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, DEFAULT_TEMPLATE);
    }

    @Test
    @Transactional
    public void getNonExistingTemplateFailsWith404() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/template/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void insertValidTemplateReturnsNewEntry() throws Exception {
        val resultActions = mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/template")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_VALID_TEMPLATE)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, NEW_VALID_TEMPLATE);
    }

    @Test
    @Transactional
    public void insertInvalidTemplateReturns400() throws Exception {
        for(TemplateRest t : NEW_INVALID_TEMPLATES) {
            val resultActions = mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/template")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsBytes(t)))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @Transactional
    public void insertDuplicateTemplateReturnsConflict() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/template")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_DUPLICATE_TEMPLATE)))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    public void updateValidTemplateReturnsNewData() throws Exception {
        val resultActions = mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/template/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(UPDATE_VALID_TEMPLATE)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, UPDATE_VALID_TEMPLATE);
    }

    @Test
    @Transactional
    public void updateInvalidTemplateReturns400() throws Exception {
        for(TemplateRest t : UPDATE_INVALID_TEMPLATES) {
            mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/template/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsBytes(t)))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @Transactional
    public void deleteExistingTemplateReturns200AndMarksAsDeletedAndNoLongerLists() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/i18n/admin/template/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/template/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleted").value(true));

        mvc.perform(MockMvcRequestBuilders.get("/i18n/translator/template")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(0));
    }

    @Test
    @Transactional
    public void deleteNonExistingTemplateReturns200() throws Exception {
        val resultActions = mvc.perform(MockMvcRequestBuilders.delete("/i18n/admin/template/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
