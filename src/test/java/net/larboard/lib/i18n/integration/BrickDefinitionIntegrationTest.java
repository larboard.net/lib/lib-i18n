package net.larboard.lib.i18n.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static net.larboard.lib.i18n.integration.matcher.BrickDefinitionRestMatcher.expectToBeLike;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.NEW_INVALID_CONFLICTING_DEFINITION;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.NEW_INVALID_DEFINITIONS;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.NEW_INVALID_TEMPLATE_DEFINITION;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.NEW_VALID_DEFINITION;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.UPDATE_INVALID_DEFINITIONS;
import static net.larboard.lib.i18n.integration.mock.BrickDefinitionMocks.UPDATE_VALID_DEFINITION;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BrickDefinitionIntegrationTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    /* TODO:
    test filtered list
     */

    @Test
    @Transactional
    public void getExistingDefinitionReturnsDefinition() throws Exception {
        _insertValidEntry();

        val resultActions = mvc.perform(MockMvcRequestBuilders.get("/i18n/admin/brick/{template}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, NEW_VALID_DEFINITION);
    }

    @Test
    @Transactional
    public void getNonExistingDefinitionFailsWith404() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/i18n/admin/brick/1/blabla.brick")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void insertValidDefinitionReturnsNewEntry() throws Exception {
        val resultActions = mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_VALID_DEFINITION)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, NEW_VALID_DEFINITION);
    }

    @Test
    @Transactional
    public void insertInvalidDefinitionReturns400() throws Exception {
        for(BrickDefinitionRest d : NEW_INVALID_DEFINITIONS) {
            mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsBytes(d)))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @Transactional
    public void insertInvalidTemplateReferenceDefinitionReturns404() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_INVALID_TEMPLATE_DEFINITION)))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void insertConflictingDefinitionReturnsConflict() throws Exception {
        _insertValidEntry();

        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_INVALID_CONFLICTING_DEFINITION)))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    public void insertDuplicateDefinitionReturnsConflict() throws Exception {
        _insertValidEntry();

        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_VALID_DEFINITION)))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    public void updateValidDefinitionReturnsNewEntry() throws Exception {
        _insertValidEntry();

        val resultActions = mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/brick/{tpl}/{key}", UPDATE_VALID_DEFINITION.getTemplate(), UPDATE_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(UPDATE_VALID_DEFINITION)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        expectToBeLike(resultActions, UPDATE_VALID_DEFINITION);
    }

    @Test
    @Transactional
    public void updateNonExistingDefinitionReturns404() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/brick/{tpl}/{key}", UPDATE_VALID_DEFINITION.getTemplate(), UPDATE_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(UPDATE_VALID_DEFINITION)))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvalidDefinitionReturns400() throws Exception {
        _insertValidEntry();

        for(BrickDefinitionRest d : UPDATE_INVALID_DEFINITIONS) {
            mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/brick/{tpl}/{key}", d.getTemplate(), d.getKey())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsBytes(d)))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @Transactional
    public void deleteExistingDefinitionReturns200AndMarksAsDeleted() throws Exception {
        _insertValidEntry();

        mvc.perform(MockMvcRequestBuilders.delete("/i18n/admin/brick/{tpl}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.get("/i18n/admin/brick/{tpl}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleted").value(true));
    }

    @Test
    @Transactional
    public void deleteExistingDefinitionIsGoneAfterRelease() throws Exception {
        _insertValidEntry();

        mvc.perform(MockMvcRequestBuilders.delete("/i18n/admin/brick/{tpl}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(MockMvcRequestBuilders.put("/i18n/admin/template/{tpl}/release", NEW_VALID_DEFINITION.getTemplate()));

        mvc.perform(MockMvcRequestBuilders.get("/i18n/admin/brick/{tpl}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteNonExistingDefinitionReturns200() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/i18n/admin/brick/{tpl}/{key}", NEW_VALID_DEFINITION.getTemplate(), NEW_VALID_DEFINITION.getKey())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private void _insertValidEntry() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/i18n/admin/brick")
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsBytes(NEW_VALID_DEFINITION)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
}
