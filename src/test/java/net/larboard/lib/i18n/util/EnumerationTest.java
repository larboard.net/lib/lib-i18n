package net.larboard.lib.i18n.util;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EnumerationTest {
    private static class TestEnum {
        public static final String VALUE_A = "VALUE_A";
        public static final String VALUE_B = "VALUE_B";
        public static final String VALUE_C = "VALUE_C";
    }

    private static class InvalidTestEnum {
        public static final String VALUE_A = "VALUE_A";
        public static final Long VALUE_B = 30L;
        public static final String VALUE_C = "VALUE_C";
    }

    private static class TestEnumEmpty {}

    @Test
    public void valueIsValid() {
        assertTrue(Enumeration.isValid("VALUE_A", TestEnum.class));
        assertTrue(Enumeration.isValid("VALUE_B", TestEnum.class));
        assertTrue(Enumeration.isValid("VALUE_C", TestEnum.class));
    }

    @Test
    public void valueIsInvalid() {
        assertFalse(Enumeration.isValid("VALUE_D", TestEnum.class));
    }

    @Test
    public void listContainsAll() {
        List<String> res = Enumeration.values(TestEnum.class);

        assertEquals(3, res.size());
        assertTrue(res.contains("VALUE_A"));
        assertTrue(res.contains("VALUE_B"));
        assertTrue(res.contains("VALUE_C"));
    }

    @Test
    public void emptyListIfNoProperties() {
        List<String> res = Enumeration.values(TestEnumEmpty.class);

        assertNotNull(res);
        assertTrue(res.isEmpty());
    }

    @Test
    public void invalidTestEnumThrows() {
        assertThrows(
                RuntimeException.class,
                () -> {
                    Enumeration.isValid("TEST", InvalidTestEnum.class);
                }
        );

        assertThrows(
                RuntimeException.class,
                () -> {
                    Enumeration.values(InvalidTestEnum.class);
                }
        );
    }
}