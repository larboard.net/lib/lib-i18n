package net.larboard.lib.i18n;

import net.larboard.lib.i18n.exception.controller.CustomErrorController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
@EnableAutoConfiguration
@EnableTranslationService
@EnableTranslationMigrations
public class TranslationTestApplication {

    @Bean
    public CustomErrorController customErrorController() {
        return new CustomErrorController();
    }

    public static void main(String[] args) {
        SpringApplication.run(TranslationTestApplication.class, args);
    }

}
