ALTER TABLE translation_template CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE translation_brick_definition CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE translation_brick_translation_dist CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE translation_brick_translation_next CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
