package net.larboard.lib.i18n.service.impl;

import net.larboard.lib.i18n.converter.BrickTranslationConverter;
import net.larboard.lib.i18n.converter.BrickTranslationNextConverter;
import net.larboard.lib.i18n.data.dto.BrickTranslationDto;
import net.larboard.lib.i18n.enumeration.DistFormat;
import net.larboard.lib.i18n.repository.BrickTranslationNextRepository;
import net.larboard.lib.i18n.repository.BrickTranslationRepository;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.service.TranslationDistService;
import net.larboard.lib.i18n.service.format.JsonExportFormat;
import net.larboard.lib.i18n.service.format.JsonFlatExportFormat;
import net.larboard.lib.i18n.service.format.XlifExportFormat;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TranslationDistServiceImpl implements TranslationDistService {
    private static final JsonExportFormat JSON_EXPORT_FORMAT = new JsonExportFormat();
    private static final JsonFlatExportFormat JSON_FLAT_EXPORT_FORMAT = new JsonFlatExportFormat();
    private static final XlifExportFormat XLIF_EXPORT_FORMAT = new XlifExportFormat();

    private final TemplateService templateService;

    private final BrickTranslationRepository brickTranslationRepository;
    private final BrickTranslationNextRepository brickTranslationNextRepository;

    private final BrickTranslationConverter brickTranslationConverter;
    private final BrickTranslationNextConverter brickTranslationNextConverter;

    public TranslationDistServiceImpl(TemplateService templateService, BrickTranslationRepository brickTranslationRepository, BrickTranslationNextRepository brickTranslationNextRepository, BrickTranslationConverter brickTranslationConverter, BrickTranslationNextConverter brickTranslationNextConverter) {
        this.templateService = templateService;
        this.brickTranslationRepository = brickTranslationRepository;
        this.brickTranslationNextRepository = brickTranslationNextRepository;
        this.brickTranslationConverter = brickTranslationConverter;
        this.brickTranslationNextConverter = brickTranslationNextConverter;
    }

    @Override
    public Object getDist(String template, String language, String format) {
        Long templateId = templateService.getIdForSlug(template);

        List<BrickTranslationDto> all = listAllBricksForLanguageAndTemplate(language, templateId, true);

        return serializeToSelectedFormat(all, format);
    }

    @Override
    public Object getNext(String template, String language, String format) {
        Long templateId = templateService.getIdForSlug(template);

        List<BrickTranslationDto> dist = listAllBricksForLanguageAndTemplate(language, templateId, false);
        List<BrickTranslationDto> next = listAllNextBricksForLanguageAndTemplate(language, templateId);

        Map<String, BrickTranslationDto> mapped = dist.stream().collect(
                Collectors.toMap(BrickTranslationDto::getKey, entry -> entry)
        );

        for(BrickTranslationDto entry : next) {
            if(mapped.containsKey(entry.getKey())) {
                mapped.get(entry.getKey()).setValue(entry.getValue());
            }
            else {
                mapped.put(entry.getKey(), entry);
            }
        }

        List<BrickTranslationDto> all = new ArrayList<>(mapped.values());

        return serializeToSelectedFormat(all, format);
    }

    private Object serializeToSelectedFormat(List<BrickTranslationDto> translations, String format) {
        switch (format) {
            case DistFormat.XLIF:
                return XLIF_EXPORT_FORMAT.serialize(translations);
            case DistFormat.JSON_FLAT:
                return JSON_FLAT_EXPORT_FORMAT.serialize(translations);
            case DistFormat.JSON:
            default:
                return JSON_EXPORT_FORMAT.serialize(translations);
        }
    }

    private List<BrickTranslationDto> listAllBricksForLanguageAndTemplate(final String language, final Long templateId, final boolean includeDeleted) {
        Boolean deleted = false;
        if(includeDeleted) {
            deleted = null;
        }

        return brickTranslationConverter.modelToDto(
                brickTranslationRepository.findAllForLanguageAndTemplate(language, templateId, deleted)
        );
    }

    private List<BrickTranslationDto> listAllNextBricksForLanguageAndTemplate(final String language, final Long templateId) {
        return brickTranslationNextConverter.modelToDto(
                brickTranslationNextRepository.findAllForLanguageAndTemplate(language, templateId)
        );
    }
}
