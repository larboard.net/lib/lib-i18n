package net.larboard.lib.i18n.service.impl;

import net.larboard.lib.i18n.converter.TemplateConverter;
import net.larboard.lib.i18n.data.api.filter.TemplateFilter;
import net.larboard.lib.i18n.data.api.post.TemplateCreateRequest;
import net.larboard.lib.i18n.data.api.put.TemplateUpdateRequest;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import net.larboard.lib.i18n.data.model.TemplateModel;
import net.larboard.lib.i18n.exception.impl.ConflictException;
import net.larboard.lib.i18n.exception.impl.EntryNotFoundException;
import net.larboard.lib.i18n.factory.TemplateFactory;
import net.larboard.lib.i18n.repository.TemplateRepository;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.service.TranslationService;
import net.larboard.lib.i18n.validator.TemplateValidator;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class TemplateServiceImpl implements TemplateService {
    private final TemplateRepository templateRepository;
    private final TranslationService translationService;

    private final TemplateConverter templateConverter;

    private final TemplateFactory templateFactory;
    private final TemplateValidator templateValidator;

    public TemplateServiceImpl(TemplateRepository templateRepository, TranslationService translationService, TemplateConverter templateConverter, TemplateFactory templateFactory, TemplateValidator templateValidator) {
        this.templateRepository = templateRepository;
        this.translationService = translationService;
        this.templateConverter = templateConverter;
        this.templateFactory = templateFactory;
        this.templateValidator = templateValidator;
    }

    @Override
    public Page<TemplateDto> listPagedAndFiltered(Pageable pageRequest, TemplateFilter filter) {
        Page<TemplateModel> result = templateRepository.listPagedAndFiltered(filter.getType(), pageRequest);

        return templateConverter.modelToDto(result);
    }

    @Override
    public TemplateDto getById(Long id) {
        TemplateModel model = findEntry(id);

        return templateConverter.modelToDto(model);
    }

    @Override
    public Long getIdForSlug(String slug) {
        Optional<TemplateModel> probe = templateRepository.findBySlug(slug);

        if(probe.isPresent()) {
            return probe.get().getId();
        }

        throw new RuntimeException("slug not found");
    }

    @Override
    @Transactional
    public TemplateDto create(TemplateCreateRequest request) {
        TemplateModel model = templateFactory.createFromRequest(request);

        templateValidator.validate(model);

        try {
            model = templateRepository.save(model);
        }
        catch (DataIntegrityViolationException e) {
            throw new ConflictException(e);
        }

        return templateConverter.modelToDto(model);
    }

    @Override
    @Transactional
    public TemplateDto update(Long id, TemplateUpdateRequest request) {
        TemplateModel model = findEntry(id);

        BeanUtils.copyProperties(request, model);

        templateValidator.validate(model);

        try {
            model = templateRepository.save(model);
        }
        catch (DataIntegrityViolationException e) {
            throw new ConflictException(e);
        }

        return templateConverter.modelToDto(model);
    }

    @Override
    @Transactional
    public void release(Long id) {
        TemplateModel model = findEntry(id);

        for(String lang : model.getEnabledLanguages()) {
            translationService.release(lang, id);
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        try {
            TemplateModel model = findEntry(id);

            model.setDeleted(true);

            templateRepository.save(model);
        }
        catch (Exception e) {
            // do nothing: idempotency
        }
    }

    private TemplateModel findEntry(Long id) {
        Optional<TemplateModel> probe = templateRepository.findById(id);

        if(probe.isPresent()) {
            return probe.get();
        }

        throw new EntryNotFoundException("not found: "+id);
    }
}
