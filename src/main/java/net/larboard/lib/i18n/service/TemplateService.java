package net.larboard.lib.i18n.service;

import net.larboard.lib.i18n.data.api.filter.TemplateFilter;
import net.larboard.lib.i18n.data.api.post.TemplateCreateRequest;
import net.larboard.lib.i18n.data.api.put.TemplateUpdateRequest;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TemplateService {
    Page<TemplateDto> listPagedAndFiltered(Pageable pageRequest, TemplateFilter filter);

    TemplateDto getById(Long id);

    Long getIdForSlug(String slug);

    TemplateDto create(TemplateCreateRequest request);

    TemplateDto update(Long id, TemplateUpdateRequest request);

    void release(Long id);

    void delete(Long id);
}
