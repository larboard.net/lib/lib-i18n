package net.larboard.lib.i18n.service;

public interface TranslationDistService {
    Object getDist(String template, String language, String format);

    Object getNext(String template, String language, String format);
}
