package net.larboard.lib.i18n.service.format;

import net.larboard.lib.i18n.data.dto.BrickTranslationDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonExportFormat implements ExportFormat {
    @Override
    public Object serialize(List<BrickTranslationDto> translations) {
        Map<String, Object> result = new HashMap<>();
        for(BrickTranslationDto entry : translations) {
            String[] path = entry.getKey().split("\\.");

            Map<String, Object> tmp = result;
            int i = 1;
            for(String current : path) {
                if(i == path.length) {
                    tmp.put(current, entry.getValue());
                    break;
                }

                i++;

                if(!tmp.containsKey(current)) {
                    tmp.put(current, new HashMap<>());
                }

                tmp = (Map<String, Object>) tmp.get(current);
            }
        }

        return result;
    }
}
