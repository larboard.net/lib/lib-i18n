package net.larboard.lib.i18n.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.larboard.lib.i18n.converter.BrickDefinitionConverter;
import net.larboard.lib.i18n.converter.BrickTranslationConverter;
import net.larboard.lib.i18n.converter.BrickTranslationNextConverter;
import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.data.BrickTranslationId;
import net.larboard.lib.i18n.data.api.BrickAndTranslationRest;
import net.larboard.lib.i18n.data.api.filter.BrickFilter;
import net.larboard.lib.i18n.data.api.patch.BrickTranslationUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickTranslationDto;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import net.larboard.lib.i18n.data.model.BrickTranslationModel;
import net.larboard.lib.i18n.data.model.BrickTranslationNextModel;
import net.larboard.lib.i18n.exception.impl.EntryNotFoundException;
import net.larboard.lib.i18n.exception.impl.ValidationException;
import net.larboard.lib.i18n.factory.BrickTranslationNextFactory;
import net.larboard.lib.i18n.repository.BrickDefinitionRepository;
import net.larboard.lib.i18n.repository.BrickTranslationNextRepository;
import net.larboard.lib.i18n.repository.BrickTranslationRepository;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.service.TranslationService;
import net.larboard.lib.i18n.validator.BrickTranslationNextValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class TranslationServiceImpl implements TranslationService {
    private final TemplateService templateService;

    private final BrickTranslationNextFactory brickTranslationNextFactory;
    private final BrickTranslationNextValidator brickTranslationNextValidator;

    private final BrickTranslationRepository brickTranslationRepository;
    private final BrickTranslationNextRepository brickTranslationNextRepository;
    private final BrickDefinitionRepository brickDefinitionRepository;

    private final BrickDefinitionConverter brickDefinitionConverter;
    private final BrickTranslationConverter brickTranslationConverter;
    private final BrickTranslationNextConverter brickTranslationNextConverter;

    @Autowired
    public TranslationServiceImpl(TemplateService templateService, BrickTranslationNextFactory brickTranslationNextFactory, BrickTranslationNextValidator brickTranslationNextValidator, BrickTranslationRepository brickTranslationRepository, BrickTranslationNextRepository brickTranslationNextRepository, BrickDefinitionRepository brickDefinitionRepository, BrickDefinitionConverter brickDefinitionConverter, BrickTranslationConverter brickTranslationConverter, BrickTranslationNextConverter brickTranslationNextConverter) {
        this.templateService = templateService;
        this.brickTranslationNextFactory = brickTranslationNextFactory;
        this.brickTranslationNextValidator = brickTranslationNextValidator;
        this.brickTranslationRepository = brickTranslationRepository;
        this.brickTranslationNextRepository = brickTranslationNextRepository;
        this.brickDefinitionRepository = brickDefinitionRepository;
        this.brickDefinitionConverter = brickDefinitionConverter;
        this.brickTranslationConverter = brickTranslationConverter;
        this.brickTranslationNextConverter = brickTranslationNextConverter;
    }

    @Override
    public Page<BrickAndTranslationRest> listBrickTranslationsPagedAndFiltered(Pageable pageable, Long templateId, BrickFilter filter) {
        String searchTerm = filter.getSearchTerm();
        if(searchTerm == null || searchTerm.isEmpty()) {
            searchTerm = "%";
        }
        else {
            searchTerm = "%"+searchTerm+"%";
        }

        boolean emptyOnly = false;
        if(filter.getEmptyOnly() != null && filter.getEmptyOnly()) {
            emptyOnly = true;
        }

        boolean modifiedOnly = false;
        if(filter.getModifiedOnly() != null && filter.getModifiedOnly()) {
            modifiedOnly = true;
        }

        Page<BrickDefinitionModel> bricks;
        if(emptyOnly) {
            bricks = brickDefinitionRepository.listEmptyPagedAndFiltered(templateId, searchTerm, filter.getTargetLanguage(), pageable);
        }
        else if(modifiedOnly) {
            bricks = brickDefinitionRepository.listModifiedPagedAndFiltered(templateId, searchTerm, filter.getTargetLanguage(), pageable);
        }
        else {
            bricks = brickDefinitionRepository.findAllForTemplate(templateId, searchTerm, filter.getTargetLanguage(), pageable);
        }

        if(bricks.isEmpty()) {
            return new PageImpl<>(Collections.emptyList(), pageable, bricks.getTotalElements());
        }

        List<BrickAndTranslationRest> result = aggregateTranslations(bricks, templateId, filter);

        result.sort(
                Comparator.comparing(o -> o.getDefinition().getKey())
        );

        return new PageImpl<>(result, pageable, bricks.getTotalElements());
    }

    private List<BrickAndTranslationRest> aggregateTranslations(Page<BrickDefinitionModel> bricks, Long templateId, BrickFilter filter) {
        Map<String, BrickAndTranslationRest> brickMap = new HashMap<>();

        for(BrickDefinitionModel entry : bricks) {
            BrickAndTranslationRest bt = new BrickAndTranslationRest();
            bt.setDefinition(brickDefinitionConverter.modelToRest(entry));
            bt.setTemplateId(templateId);

            brickMap.put(entry.getKey(), bt);
        }

        Set<String> brickIds = brickMap.keySet();

        List<BrickTranslationModel> sourceDist = brickTranslationRepository.findInListForTemplateAndLanguage(brickIds, templateId, filter.getSourceLanguage());
        List<BrickTranslationModel> targetDist = brickTranslationRepository.findInListForTemplateAndLanguage(brickIds, templateId, filter.getTargetLanguage());

        List<BrickTranslationNextModel> sourceNext = brickTranslationNextRepository.findInListForTemplateAndLanguage(brickIds, templateId, filter.getSourceLanguage());
        List<BrickTranslationNextModel> targetNext = brickTranslationNextRepository.findInListForTemplateAndLanguage(brickIds, templateId, filter.getTargetLanguage());

        for(BrickTranslationModel entry : sourceDist) {
            BrickAndTranslationRest bt = brickMap.get(entry.getKey());
            bt.setSourceDist(brickTranslationConverter.modelToRest(entry));
        }

        for(BrickTranslationModel entry : targetDist) {
            BrickAndTranslationRest bt = brickMap.get(entry.getKey());
            bt.setTargetDist(brickTranslationConverter.modelToRest(entry));
        }

        for(BrickTranslationNextModel entry : sourceNext) {
            BrickAndTranslationRest bt = brickMap.get(entry.getKey());
            bt.setSourceNext(brickTranslationNextConverter.modelToRest(entry));
        }

        for(BrickTranslationNextModel entry : targetNext) {
            BrickAndTranslationRest bt = brickMap.get(entry.getKey());
            bt.setTargetNext(brickTranslationNextConverter.modelToRest(entry));
        }

        return new ArrayList<>(brickMap.values());
    }

    @Override
    public BrickTranslationDto saveTranslation(final BrickTranslationUpdateRequest request) {
        Optional<BrickTranslationNextModel> probe = brickTranslationNextRepository.findById(
                BrickTranslationId.of(
                        request.getKey(),
                        request.getLang(),
                        request.getTemplate()
                )
        );

        BrickTranslationNextModel model;
        if(probe.isPresent()) {
            model = probe.get();

            model.setValue(request.getValue());
        }
        else {
            model = brickTranslationNextFactory.createFromRequest(request);

            brickTranslationNextValidator.validate(model);

            TemplateDto template = templateService.getById(model.getTemplate());

            if(!template.getEnabledLanguages().contains(model.getLang())) {
                throw new ValidationException("lang not allowed");
            }

            Optional<BrickDefinitionModel> definitionProbe = brickDefinitionRepository.findById(BrickDefinitionId.of(model.getKey(), model.getTemplate()));
            if(definitionProbe.isEmpty()) {
                throw new EntryNotFoundException("invalid brick");
            }
        }

        model = brickTranslationNextRepository.save(model);

        return brickTranslationNextConverter.modelToDto(model);
    }

    private void deleteIfExists(@NonNull final BrickTranslationId id) {
        Optional<BrickTranslationModel> probe = brickTranslationRepository.findById(id);

        if(probe.isPresent()) {
            brickTranslationRepository.deleteById(id);
        }
    }

    private void deleteNextIfExists(@NonNull final BrickTranslationId id) {
        Optional<BrickTranslationNextModel> probe = brickTranslationNextRepository.findById(id);

        if(probe.isPresent()) {
            brickTranslationNextRepository.deleteById(id);
        }
    }

    @Override
    @Transactional
    public void release(String lang, Long templateId) {
        List<BrickTranslationDto> next = listAllNextBricksForLanguageAndTemplate(lang, templateId);

        for(BrickTranslationDto entry : next) {
            if(entry.getValue() == null || entry.getValue().isEmpty()) {
                deleteIfExists(
                        BrickTranslationId.of(entry.getKey(), entry.getLang(), entry.getTemplate())
                );
            }
            else {
                brickTranslationRepository.save(brickTranslationConverter.dtoToModel(entry));
                deleteNextIfExists(
                        BrickTranslationId.of(entry.getKey(), entry.getLang(), entry.getTemplate())
                );
            }
        }

        // delete marked bricks and delete all orphaned translations
        brickDefinitionRepository.realDeletedAllMarked(templateId);
        brickTranslationRepository.deleteOrphans();
        brickTranslationNextRepository.deleteOrphans();
    }

    private List<BrickTranslationDto> listAllNextBricksForLanguageAndTemplate(final String language, final Long templateId) {
        return brickTranslationNextConverter.modelToDto(
                brickTranslationNextRepository.findAllForLanguageAndTemplate(language, templateId)
        );
    }
}
