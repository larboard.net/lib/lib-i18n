package net.larboard.lib.i18n.service.format;

import net.larboard.lib.i18n.data.dto.BrickTranslationDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonFlatExportFormat implements ExportFormat {
    @Override
    public Object serialize(List<BrickTranslationDto> translations) {
        Map<String, String> result = new HashMap<>();
        for(BrickTranslationDto entry : translations) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}
