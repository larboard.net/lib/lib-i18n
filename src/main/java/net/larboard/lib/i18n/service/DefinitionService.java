package net.larboard.lib.i18n.service;

import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.data.api.filter.BrickDefinitionFilter;
import net.larboard.lib.i18n.data.api.post.BrickDefinitionCreateRequest;
import net.larboard.lib.i18n.data.api.put.BrickDefinitionUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickDefinitionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DefinitionService {
    Page<BrickDefinitionDto> listBricksPagedAndFiltered(Long templateId, Pageable pageable, BrickDefinitionFilter filter);

    BrickDefinitionDto createBrickDefinition(BrickDefinitionCreateRequest request);

    BrickDefinitionDto updateBrickDefinition(BrickDefinitionId brickId, BrickDefinitionUpdateRequest request);

    void deleteBrickDefinition(BrickDefinitionId brickId);

    BrickDefinitionDto getById(BrickDefinitionId brickId);
}
