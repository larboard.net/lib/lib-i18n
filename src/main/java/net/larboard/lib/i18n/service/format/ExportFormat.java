package net.larboard.lib.i18n.service.format;

import net.larboard.lib.i18n.data.dto.BrickTranslationDto;

import java.util.List;

public interface ExportFormat {
    Object serialize(List<BrickTranslationDto> translations);
}
