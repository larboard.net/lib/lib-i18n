package net.larboard.lib.i18n.service;

import net.larboard.lib.i18n.data.api.BrickAndTranslationRest;
import net.larboard.lib.i18n.data.api.filter.BrickFilter;
import net.larboard.lib.i18n.data.api.patch.BrickTranslationUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickTranslationDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TranslationService {
    Page<BrickAndTranslationRest> listBrickTranslationsPagedAndFiltered(Pageable pageable, Long templateId, BrickFilter filter);

    BrickTranslationDto saveTranslation(BrickTranslationUpdateRequest request);

    void release(String lang, Long templateId);
}
