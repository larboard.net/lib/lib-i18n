package net.larboard.lib.i18n.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.larboard.lib.i18n.converter.BrickDefinitionConverter;
import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.data.api.filter.BrickDefinitionFilter;
import net.larboard.lib.i18n.data.api.post.BrickDefinitionCreateRequest;
import net.larboard.lib.i18n.data.api.put.BrickDefinitionUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickDefinitionDto;
import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import net.larboard.lib.i18n.exception.impl.ConflictException;
import net.larboard.lib.i18n.exception.impl.EntryNotFoundException;
import net.larboard.lib.i18n.factory.BrickDefinitionFactory;
import net.larboard.lib.i18n.repository.BrickDefinitionRepository;
import net.larboard.lib.i18n.service.DefinitionService;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.validator.BrickDefinitionValidator;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Slf4j
public class DefinitionServiceImpl implements DefinitionService {
    private final BrickDefinitionRepository brickDefinitionRepository;
    private final BrickDefinitionConverter brickDefinitionConverter;

    private final BrickDefinitionFactory brickDefinitionFactory;
    private final BrickDefinitionValidator brickDefinitionValidator;

    private final TemplateService templateService;

    public DefinitionServiceImpl(BrickDefinitionRepository brickDefinitionRepository, BrickDefinitionConverter brickDefinitionConverter, BrickDefinitionFactory brickDefinitionFactory, BrickDefinitionValidator brickDefinitionValidator, TemplateService templateService) {
        this.brickDefinitionRepository = brickDefinitionRepository;
        this.brickDefinitionConverter = brickDefinitionConverter;
        this.brickDefinitionFactory = brickDefinitionFactory;
        this.brickDefinitionValidator = brickDefinitionValidator;
        this.templateService = templateService;
    }

    @Override
    public Page<BrickDefinitionDto> listBricksPagedAndFiltered(Long templateId, Pageable pageable, BrickDefinitionFilter filter) {
        String searchTerm = filter.getSearchTerm();
        if (searchTerm == null || searchTerm.isBlank()) {
            searchTerm = "%";
        } else {
            searchTerm = "%" + searchTerm + "%";
        }

        Page<BrickDefinitionModel> result = brickDefinitionRepository.listPagedAndFiltered(templateId, searchTerm, pageable);

        return brickDefinitionConverter.modelToDto(result);
    }

    @Override
    public BrickDefinitionDto getById(BrickDefinitionId brickId) {
        return brickDefinitionConverter.modelToDto(findModel(brickId));
    }

    @Override
    @Transactional
    public void deleteBrickDefinition(BrickDefinitionId brickId) {
        try {
            BrickDefinitionModel model = findModel(brickId);

            model.setDeleted(true);

            brickDefinitionRepository.save(model);
        }
        catch (EntryNotFoundException e) {
            // idempotency
        }
    }

    @Override
    @Transactional
    public BrickDefinitionDto createBrickDefinition(BrickDefinitionCreateRequest request) {
        BrickDefinitionModel model = brickDefinitionFactory.createFromRequest(request);

        brickDefinitionValidator.validate(model);

        checkIfTemplateExists(model.getTemplate());

        checkForKeyViolation(model);
        checkForStructuralParentViolation(model);

        model = brickDefinitionRepository.save(model);

        return brickDefinitionConverter.modelToDto(model);
    }

    @Override
    @Transactional
    public BrickDefinitionDto updateBrickDefinition(BrickDefinitionId brickId, BrickDefinitionUpdateRequest request) {
        BrickDefinitionModel model = findModel(brickId);

        BeanUtils.copyProperties(request, model);

        brickDefinitionValidator.validate(model);

        model = brickDefinitionRepository.save(model);

        return brickDefinitionConverter.modelToDto(model);
    }

    private BrickDefinitionModel findModel(BrickDefinitionId brickId) {
        Optional<BrickDefinitionModel> probe = brickDefinitionRepository.findById(brickId);

        if (probe.isEmpty()) {
            throw new EntryNotFoundException("does not exist");
        }

        return probe.get();
    }

    private void checkIfTemplateExists(Long templateId) {
        templateService.getById(templateId); // throws EntryNotFoundException
    }

    private void checkForKeyViolation(BrickDefinitionModel model) {
        Optional<BrickDefinitionModel> probe = brickDefinitionRepository.findById(
                BrickDefinitionId.of(
                        model.getKey(),
                        model.getTemplate()
                )
        );

        if (probe.isPresent()) {
            throw new ConflictException("already exists");
        }
    }

    private void checkForStructuralParentViolation(BrickDefinitionModel model) {
        //TODO: this is wrong! is must check for any parent not just the last one
        int i = model.getKey().lastIndexOf('.');
        if (i > 0) {
            String keyBefore = model.getKey().substring(0, i);

            Optional<BrickDefinitionModel> probe2 = brickDefinitionRepository.findById(
                    BrickDefinitionId.of(
                            keyBefore,
                            model.getTemplate()
                    )
            );

            if (probe2.isPresent()) {
                throw new ConflictException("direct parent exists");
            }
        }
    }
}
