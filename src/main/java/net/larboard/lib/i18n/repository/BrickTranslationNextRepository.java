package net.larboard.lib.i18n.repository;

import net.larboard.lib.i18n.data.BrickTranslationId;
import net.larboard.lib.i18n.data.model.BrickTranslationNextModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Repository
public interface BrickTranslationNextRepository extends JpaRepository<BrickTranslationNextModel, BrickTranslationId> {
    @Query("SELECT t FROM BrickTranslationNextModel t, BrickDefinitionModel d WHERE t.lang=?1 AND t.template=?2 AND d.key=t.key AND d.template=t.template AND d.deleted=false")
    List<BrickTranslationNextModel> findAllForLanguageAndTemplate(String language, Long templateId);

    @Query("SELECT t FROM BrickTranslationNextModel t, BrickDefinitionModel d WHERE t.key IN ?1 AND t.template=?2 AND t.lang=?3 AND d.key=t.key AND d.template=t.template AND d.deleted=false")
    List<BrickTranslationNextModel> findInListForTemplateAndLanguage(Set<String> brickIds, Long templateId, String language);

    @Transactional
    @Modifying
    @Query("DELETE FROM BrickTranslationNextModel t WHERE NOT EXISTS (SELECT d FROM BrickDefinitionModel d WHERE d.template=t.template AND d.key=t.key)")
    void deleteOrphans();
}
