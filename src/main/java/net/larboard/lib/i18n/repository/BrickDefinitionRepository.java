package net.larboard.lib.i18n.repository;

import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface BrickDefinitionRepository extends JpaRepository<BrickDefinitionModel, BrickDefinitionId> {
    @Query("SELECT d FROM BrickDefinitionModel d WHERE d.template=?1 AND (d.key LIKE ?2 OR EXISTS(SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.value LIKE ?2) OR EXISTS(SELECT tn.key FROM BrickTranslationModel tn WHERE tn.key=d.key AND tn.value LIKE ?2)) AND d.deleted=false ORDER BY d.key ASC")
    Page<BrickDefinitionModel> listPagedAndFiltered(Long templateId, String searchTerm, Pageable pageable);

    @Query("SELECT d FROM BrickDefinitionModel d WHERE d.template=?1 AND (d.key LIKE ?2 OR EXISTS(SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2) OR EXISTS(SELECT tn.key FROM BrickTranslationModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2)) AND NOT EXISTS (SELECT t.key FROM BrickTranslationModel t WHERE t.key=d.key AND t.lang=?3) AND NOT EXISTS (SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.lang=?3) AND d.deleted=false ORDER BY d.key ASC")
    Page<BrickDefinitionModel> listEmptyPagedAndFiltered(Long templateId, String searchTerm, String targetLanguage, Pageable pageable);

    @Query("SELECT d FROM BrickDefinitionModel d WHERE d.template=?1 AND (d.key LIKE ?2 OR EXISTS(SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2) OR EXISTS(SELECT tn.key FROM BrickTranslationModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2)) AND EXISTS (SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.lang=?3) AND d.deleted=false ORDER BY d.key ASC")
    Page<BrickDefinitionModel> listModifiedPagedAndFiltered(Long templateId, String searchTerm, String targetLanguage, Pageable pageable);

    @Query("SELECT d FROM BrickDefinitionModel d WHERE d.template=?1 AND (d.key LIKE ?2 OR EXISTS(SELECT tn.key FROM BrickTranslationNextModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2) OR EXISTS(SELECT tn.key FROM BrickTranslationModel tn WHERE tn.key=d.key AND tn.lang=?3 AND tn.value LIKE ?2)) AND d.deleted=false ORDER BY d.key ASC")
    Page<BrickDefinitionModel> findAllForTemplate(Long templateId, String searchTerm, String targetLanguage, Pageable pageable);

    @Transactional
    @Modifying
    @Query("DELETE FROM BrickDefinitionModel d WHERE d.deleted=true AND d.template=?1")
    void realDeletedAllMarked(Long templateId);
}
