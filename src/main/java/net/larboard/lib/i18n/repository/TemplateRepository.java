package net.larboard.lib.i18n.repository;

import net.larboard.lib.i18n.data.model.TemplateModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TemplateRepository extends JpaRepository<TemplateModel, Long> {
    @Query("SELECT m FROM TemplateModel m WHERE m.type=?1 OR ?1 IS NULL AND m.deleted=false")
    Page<TemplateModel> listPagedAndFiltered(String type, Pageable pageable);

    @Query("SELECT m FROM TemplateModel m WHERE m.slug=?1 AND m.deleted=false")
    Optional<TemplateModel> findBySlug(String slug);
}
