package net.larboard.lib.i18n.repository;

import net.larboard.lib.i18n.data.BrickTranslationId;
import net.larboard.lib.i18n.data.model.BrickTranslationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Repository
public interface BrickTranslationRepository extends JpaRepository<BrickTranslationModel, BrickTranslationId> {
    @Query("SELECT t FROM BrickTranslationModel t, BrickDefinitionModel d WHERE t.lang=?1 AND t.template=?2 AND d.key=t.key AND d.template=t.template AND (?3 IS NULL OR d.deleted=?3)")
    List<BrickTranslationModel> findAllForLanguageAndTemplate(String language, Long templateId, Boolean deleted);

    @Query("SELECT t FROM BrickTranslationModel t WHERE t.key IN ?1 AND t.template=?2 AND t.lang=?3")
    List<BrickTranslationModel> findInListForTemplateAndLanguage(Set<String> brickIds, Long templateId, String language);

    @Transactional
    @Modifying
    @Query("DELETE FROM BrickDefinitionModel t WHERE NOT EXISTS (SELECT d FROM BrickDefinitionModel d WHERE d.template=t.template AND d.key=t.key)")
    void deleteOrphans();
}
