package net.larboard.lib.i18n.exception.impl;

import lombok.Getter;
import net.larboard.lib.i18n.exception.CoreException;
import org.springframework.http.HttpStatus;

public class EntryNotFoundException extends CoreException {
    @Getter
    private final HttpStatus httpStatus = HttpStatus.NOT_FOUND; // don't make static: breaks inherited abstract getter

    public EntryNotFoundException(String message) {
        super(message);
    }

    public EntryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntryNotFoundException(Throwable cause) {
        super(cause);
    }
}
