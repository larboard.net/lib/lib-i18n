package net.larboard.lib.i18n.exception.impl;

import lombok.Getter;
import net.larboard.lib.i18n.exception.CoreException;
import org.springframework.http.HttpStatus;

public class ConflictException extends CoreException {
    @Getter
    private final HttpStatus httpStatus = HttpStatus.CONFLICT; // don't make static: breaks inherited abstract getter

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictException(Throwable cause) {
        super(cause);
    }
}
