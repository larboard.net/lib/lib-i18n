package net.larboard.lib.i18n.hibernate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Converter
public class StringArrayConverter implements AttributeConverter<List<String>, String> {
    @Override
    public String convertToDatabaseColumn(List<String> strings) {
        return strings == null ? null : String.join("::", strings);
    }

    @Override
    public List<String> convertToEntityAttribute(String s) {
        if (s == null || s.isEmpty() || s.isBlank())
            return Collections.emptyList();

        try (Stream<String> stream = Arrays.stream(s.split("::"))) {
            return stream.map(String::valueOf).collect(Collectors.toList());
        }
    }
}
