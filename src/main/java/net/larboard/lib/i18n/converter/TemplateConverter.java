package net.larboard.lib.i18n.converter;

import net.larboard.lib.i18n.data.api.TemplateRest;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import net.larboard.lib.i18n.data.model.TemplateModel;
import net.larboard.lib.tier3.converter.impl.SimpleModelToDtoToRestConverter;
import org.springframework.stereotype.Component;

@Component
public class TemplateConverter extends SimpleModelToDtoToRestConverter<TemplateModel, TemplateDto, TemplateRest> {
}
