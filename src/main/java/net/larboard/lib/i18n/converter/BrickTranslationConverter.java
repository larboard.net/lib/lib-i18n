package net.larboard.lib.i18n.converter;

import net.larboard.lib.i18n.data.api.BrickTranslationRest;
import net.larboard.lib.i18n.data.dto.BrickTranslationDto;
import net.larboard.lib.i18n.data.model.BrickTranslationModel;
import net.larboard.lib.tier3.converter.impl.SimpleModelToDtoToRestConverter;
import org.springframework.stereotype.Component;

@Component
public class BrickTranslationConverter extends SimpleModelToDtoToRestConverter<BrickTranslationModel, BrickTranslationDto, BrickTranslationRest> {
}
