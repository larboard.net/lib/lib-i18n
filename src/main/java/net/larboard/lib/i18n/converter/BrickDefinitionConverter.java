package net.larboard.lib.i18n.converter;

import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import net.larboard.lib.i18n.data.dto.BrickDefinitionDto;
import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import net.larboard.lib.tier3.converter.impl.SimpleModelToDtoToRestConverter;
import org.springframework.stereotype.Component;

@Component
public class BrickDefinitionConverter extends SimpleModelToDtoToRestConverter<BrickDefinitionModel, BrickDefinitionDto, BrickDefinitionRest> {
}
