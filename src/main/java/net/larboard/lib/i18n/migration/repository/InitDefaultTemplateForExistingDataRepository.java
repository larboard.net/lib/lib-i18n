package net.larboard.lib.i18n.migration.repository;

import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class InitDefaultTemplateForExistingDataRepository {
    private final JdbcConnection databaseConnection;

    public InitDefaultTemplateForExistingDataRepository(JdbcConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    public boolean hasExistingTemplates() throws DatabaseException, SQLException {
        String query = "SELECT COUNT(*) as entry_count FROM translation_template;";

        Statement statement = databaseConnection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        if(resultSet.next()) {
            long count = resultSet.getLong("entry_count");

            return count != 0;
        }

        throw new DatabaseException("no or empty resultset");
    }

    public long createDefaultTemplate() throws DatabaseException, SQLException {
        String langQuery = "SELECT DISTINCT lang FROM translation_brick_translation_dist;";

        List<String> languages = new ArrayList<>();

        Statement langStatement = databaseConnection.createStatement();
        ResultSet langResultSet = langStatement.executeQuery(langQuery);
        while(langResultSet.next()) {
            languages.add(langResultSet.getString("lang"));
        }

        String defaultLang = "en";
        if(!languages.isEmpty()) {
            defaultLang = languages.get(0);
        }
        else {
            languages.add("en");
        }
        String enabledLang = String.join("::", languages);


        String insertQuery = "INSERT INTO translation_template (name, slug, type, default_language, enabled_languages) VALUES ('default', 'default', 'SITE', '"+defaultLang+"', '"+enabledLang+"');";

        Statement insertStatement = databaseConnection.createStatement();
        insertStatement.executeUpdate(insertQuery);

        String readQuery = "SELECT id FROM translation_template WHERE slug='default';";

        Statement readStatement = databaseConnection.createStatement();
        ResultSet readResultSet = readStatement.executeQuery(readQuery);
        if(readResultSet.next()) {
            return readResultSet.getLong("id");
        }

        throw new DatabaseException("no or empty resultset");
    }

    public void updateBricks(long templateId) throws DatabaseException, SQLException {
        String query = "UPDATE translation_brick_definition SET template="+templateId+" WHERE template=0 OR template IS NULL;";

        Statement statement = databaseConnection.createStatement();
        statement.executeUpdate(query);
    }

    public void updateDistTranslation(long templateId) throws DatabaseException, SQLException {
        String query = "UPDATE translation_brick_translation_dist SET template="+templateId+" WHERE template=0 OR template IS NULL;";

        Statement statement = databaseConnection.createStatement();
        statement.executeUpdate(query);
    }

    public void updateNextTranslation(long templateId) throws DatabaseException, SQLException {
        String query = "UPDATE translation_brick_translation_next SET template="+templateId+" WHERE template=0 OR template IS NULL;";

        Statement statement = databaseConnection.createStatement();
        statement.executeUpdate(query);
    }
}
