package net.larboard.lib.i18n.migration;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import net.larboard.lib.i18n.migration.repository.InitDefaultTemplateForExistingDataRepository;

import java.sql.SQLException;

public class InitDefaultTemplateForExistingDataMigration implements CustomTaskChange {
    @Override
    public void execute(Database database) throws CustomChangeException {
        JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();

        InitDefaultTemplateForExistingDataRepository repository = new InitDefaultTemplateForExistingDataRepository(databaseConnection);

        try {

            //must have no templates
            if(repository.hasExistingTemplates()) {
                throw new CustomChangeException("templates not empty!");
            }

            long templateId = repository.createDefaultTemplate();

            repository.updateBricks(templateId);
            repository.updateDistTranslation(templateId);
            repository.updateNextTranslation(templateId);

        }
        catch (DatabaseException | SQLException ex) {
            throw new CustomChangeException(ex);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return "Initializing InitDefaultTemplateForExistingDataMigration done.";
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {

    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
