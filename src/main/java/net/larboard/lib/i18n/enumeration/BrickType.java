package net.larboard.lib.i18n.enumeration;

public class BrickType {
    private BrickType() {}

    public static final String PLAIN = "PLAIN";
    public static final String MARKDOWN = "MARKDOWN";
    public static final String PLURAL = "PLURAL";
}
