package net.larboard.lib.i18n.enumeration;

public class TemplateType {
    private TemplateType() {}

    public static final String SITE = "SITE";

    public static final String EMAIL = "EMAIL";

    public static final String PDF = "PDF";
}
