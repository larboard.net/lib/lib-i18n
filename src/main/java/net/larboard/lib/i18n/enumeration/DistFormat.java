package net.larboard.lib.i18n.enumeration;

public class DistFormat {
    private DistFormat() {}

    public static final String JSON = "JSON";

    public static final String JSON_FLAT = "JSON_FLAT";

    public static final String XLIF = "XLIF";
}
