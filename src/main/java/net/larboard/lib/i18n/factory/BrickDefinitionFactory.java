package net.larboard.lib.i18n.factory;

import net.larboard.lib.i18n.data.api.post.BrickDefinitionCreateRequest;
import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class BrickDefinitionFactory {
    public BrickDefinitionModel createFromRequest(BrickDefinitionCreateRequest request) {
        BrickDefinitionModel model = new BrickDefinitionModel();

        BeanUtils.copyProperties(request, model);

        model.setDeleted(false);

        return model;
    }
}
