package net.larboard.lib.i18n.factory;

import net.larboard.lib.i18n.data.api.patch.BrickTranslationUpdateRequest;
import net.larboard.lib.i18n.data.model.BrickTranslationNextModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class BrickTranslationNextFactory {
    public BrickTranslationNextModel createFromRequest(BrickTranslationUpdateRequest request) {
        BrickTranslationNextModel model = new BrickTranslationNextModel();

        BeanUtils.copyProperties(request, model);

        return model;
    }
}
