package net.larboard.lib.i18n.factory;

import net.larboard.lib.i18n.data.api.post.TemplateCreateRequest;
import net.larboard.lib.i18n.data.model.TemplateModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class TemplateFactory {
    public TemplateModel createFromRequest(TemplateCreateRequest request) {
        TemplateModel model = new TemplateModel();

        BeanUtils.copyProperties(request, model);

        model.setDeleted(false);

        return model;
    }
}
