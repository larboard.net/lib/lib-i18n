package net.larboard.lib.i18n.data.api.patch;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class BrickTranslationUpdateRequest {
    @NotEmpty
    private String key;

    @NotEmpty
    private String lang;

    @Positive
    private Long template;

    private String value;
}
