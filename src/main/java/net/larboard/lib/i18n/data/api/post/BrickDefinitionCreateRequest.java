package net.larboard.lib.i18n.data.api.post;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Getter
@Setter
public class BrickDefinitionCreateRequest {
    @NotEmpty
    private String key;

    @Positive
    private Long template;

    @NotEmpty
    private String type;

    @NotNull
    private Boolean multiline;

    private String translationHint;

    @NotNull
    private List<String> placeholders;
}
