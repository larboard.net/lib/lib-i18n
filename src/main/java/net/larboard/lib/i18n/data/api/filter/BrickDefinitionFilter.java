package net.larboard.lib.i18n.data.api.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrickDefinitionFilter {
    private String searchTerm;
}
