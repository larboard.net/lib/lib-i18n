package net.larboard.lib.i18n.data;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class BrickTranslationId implements Serializable {
    public static BrickTranslationId of(@NonNull final String key, @NonNull final String lang, @NonNull final Long template) {
        BrickTranslationId instance = new BrickTranslationId();

        instance.setKey(key);
        instance.setLang(lang);
        instance.setTemplate(template);

        return instance;
    }

    private String key;

    private String lang;

    private Long template;
}
