package net.larboard.lib.i18n.data.api;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.tier3.data.Rest;

@Getter
@Setter
public class BrickTranslationRest implements Rest {
    private String key;

    private String lang;

    private Long template;

    private String value;
}
