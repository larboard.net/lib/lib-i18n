package net.larboard.lib.i18n.data.model;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.hibernate.StringArrayConverter;
import net.larboard.lib.tier3.data.Model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Entity
@IdClass(BrickDefinitionId.class)
@Table(name = "translation_brick_definition")
public class BrickDefinitionModel implements Model {
    @Id
    @Column(name = "brick_key")
    private String key;

    @Id
    private Long template;

    private String type;

    private Boolean multiline;

    @Column(name = "translation_hint")
    private String translationHint;

    @Convert(converter = StringArrayConverter.class)
    private List<String> placeholders;

    private Boolean deleted;
}
