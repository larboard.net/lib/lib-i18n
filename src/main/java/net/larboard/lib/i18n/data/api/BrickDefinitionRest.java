package net.larboard.lib.i18n.data.api;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.tier3.data.Rest;

import java.util.List;

@Getter
@Setter
public class BrickDefinitionRest implements Rest {
    private String key;

    private Long template;

    private String type;

    private Boolean multiline;

    private String translationHint;

    private List<String> placeholders;

    private Boolean deleted;
}
