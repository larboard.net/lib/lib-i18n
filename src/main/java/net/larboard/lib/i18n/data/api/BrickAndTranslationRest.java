package net.larboard.lib.i18n.data.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrickAndTranslationRest {
    private Long templateId;

    private BrickDefinitionRest definition;

    private BrickTranslationRest sourceDist;

    private BrickTranslationRest sourceNext;

    private BrickTranslationRest targetDist;

    private BrickTranslationRest targetNext;
}
