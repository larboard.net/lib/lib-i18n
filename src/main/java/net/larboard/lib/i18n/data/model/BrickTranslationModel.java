package net.larboard.lib.i18n.data.model;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.i18n.data.BrickTranslationId;
import net.larboard.lib.tier3.data.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@IdClass(BrickTranslationId.class)
@Table(name = "translation_brick_translation_dist")
public class BrickTranslationModel implements Model {
    @Id
    @Column(name = "brick_key")
    private String key;

    @Id
    private String lang;

    @Id
    private Long template;

    private String value;
}
