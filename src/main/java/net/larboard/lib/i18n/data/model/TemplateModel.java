package net.larboard.lib.i18n.data.model;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.i18n.hibernate.StringArrayConverter;
import net.larboard.lib.tier3.data.Model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "translation_template")
public class TemplateModel implements Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String slug;

    private String type;

    @Column(name = "default_language")
    private String defaultLanguage;

    @Column(name = "enabled_languages")
    @Convert(converter = StringArrayConverter.class)
    private List<String> enabledLanguages;

    private Boolean deleted;
}
