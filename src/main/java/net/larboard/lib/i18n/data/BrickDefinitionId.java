package net.larboard.lib.i18n.data;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class BrickDefinitionId implements Serializable {
    public static BrickDefinitionId of(@NonNull final String key, @NonNull final Long template) {
        BrickDefinitionId instance = new BrickDefinitionId();

        instance.setKey(key);
        instance.setTemplate(template);

        return instance;
    }

    private String key;

    private Long template;
}
