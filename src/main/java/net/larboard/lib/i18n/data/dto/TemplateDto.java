package net.larboard.lib.i18n.data.dto;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.tier3.data.Dto;

import java.util.List;

@Getter
@Setter
public class TemplateDto implements Dto {
    private Long id;

    private String name;

    private String slug;

    private String type;

    private String defaultLanguage;

    private List<String> enabledLanguages;

    private Boolean deleted;
}
