package net.larboard.lib.i18n.data.api.put;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class TemplateUpdateRequest {
    @NotEmpty
    private String name;

    @NotEmpty
    private String type;

    @NotEmpty
    private String defaultLanguage;

    @NotNull
    private List<String> enabledLanguages;
}
