package net.larboard.lib.i18n.data.api.post;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class TemplateCreateRequest {
    @NotEmpty
    private String name;

    @NotEmpty
    private String slug;

    @NotEmpty
    private String type;

    @NotEmpty
    private String defaultLanguage;

    @NotNull
    private List<String> enabledLanguages;
}
