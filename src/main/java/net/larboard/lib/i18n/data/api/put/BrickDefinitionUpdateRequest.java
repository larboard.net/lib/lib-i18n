package net.larboard.lib.i18n.data.api.put;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class BrickDefinitionUpdateRequest {
    @NotEmpty
    private String type;

    @NotNull
    private Boolean multiline;

    private String translationHint;

    @NotNull
    private List<String> placeholders;
}
