package net.larboard.lib.i18n.data.api.filter;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BrickFilter {
    private String searchTerm;

    @NotNull
    private Boolean emptyOnly;

    @NotNull
    private Boolean modifiedOnly;

    @NotEmpty
    private String targetLanguage;

    @NotEmpty
    private String sourceLanguage;
}
