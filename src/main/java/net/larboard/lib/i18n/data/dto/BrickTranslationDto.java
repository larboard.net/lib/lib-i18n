package net.larboard.lib.i18n.data.dto;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.tier3.data.Dto;

@Getter
@Setter
public class BrickTranslationDto implements Dto {
    private String key;

    private String lang;

    private Long template;

    private String value;
}
