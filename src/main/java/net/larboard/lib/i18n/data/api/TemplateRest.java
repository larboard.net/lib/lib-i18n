package net.larboard.lib.i18n.data.api;

import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.tier3.data.Rest;

import java.util.List;

@Getter
@Setter
public class TemplateRest implements Rest {
    private Long id;

    private String name;

    private String slug;

    private String type;

    private String defaultLanguage;

    private List<String> enabledLanguages;

    private Boolean deleted;
}
