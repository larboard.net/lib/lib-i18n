package net.larboard.lib.i18n;

import net.larboard.lib.i18n.controller.BrickTranslationAdminController;
import net.larboard.lib.i18n.controller.BrickTranslationPublicController;
import net.larboard.lib.i18n.controller.BrickTranslationTranslatorController;
import net.larboard.lib.i18n.converter.BrickDefinitionConverter;
import net.larboard.lib.i18n.converter.BrickTranslationConverter;
import net.larboard.lib.i18n.converter.BrickTranslationNextConverter;
import net.larboard.lib.i18n.converter.TemplateConverter;
import net.larboard.lib.i18n.factory.BrickDefinitionFactory;
import net.larboard.lib.i18n.factory.BrickTranslationNextFactory;
import net.larboard.lib.i18n.factory.TemplateFactory;
import net.larboard.lib.i18n.repository.BrickDefinitionRepository;
import net.larboard.lib.i18n.repository.BrickTranslationNextRepository;
import net.larboard.lib.i18n.repository.BrickTranslationRepository;
import net.larboard.lib.i18n.repository.TemplateRepository;
import net.larboard.lib.i18n.service.DefinitionService;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.service.TranslationDistService;
import net.larboard.lib.i18n.service.TranslationService;
import net.larboard.lib.i18n.service.impl.DefinitionServiceImpl;
import net.larboard.lib.i18n.service.impl.TemplateServiceImpl;
import net.larboard.lib.i18n.service.impl.TranslationDistServiceImpl;
import net.larboard.lib.i18n.service.impl.TranslationServiceImpl;
import net.larboard.lib.i18n.validator.BrickDefinitionValidator;
import net.larboard.lib.i18n.validator.BrickTranslationNextValidator;
import net.larboard.lib.i18n.validator.TemplateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "i18nDbEntityManagerFactory",
        transactionManagerRef = "i18nDbTransactionManager",
        basePackages = {"net.larboard.lib.i18n"}
)
@EnableTransactionManagement
public class TranslationServiceConfiguration {
    @Autowired
    private Environment env;

    @Bean(name = "i18nDbDataSource")
    public DataSource i18nDbDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        Boolean useDedicatedDatabase = env.getProperty("lib.i18n.dedicated-database", Boolean.class);
        if(useDedicatedDatabase == null) {
            useDedicatedDatabase = false;
        }

        String url;
        String username;
        String password;

        if(useDedicatedDatabase) {
            url = env.getProperty("lib.i18n.datasource.url");
            username = env.getProperty("lib.i18n.datasource.username");
            password = env.getProperty("lib.i18n.datasource.password");
        }
        else {
            url = env.getProperty("spring.datasource.url");
            username = env.getProperty("spring.datasource.username");
            password = env.getProperty("spring.datasource.password");
        }

        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean(name = "i18nDbEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean i18nDbEntityManagerFactory(@Qualifier("i18nDbDataSource") DataSource dataSource)  {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();

        em.setDataSource(dataSource);
        em.setPackagesToScan("net.larboard.lib.i18n");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        return em;
    }

    @Bean(name = "i18nDbTransactionManager")
    public PlatformTransactionManager i18nDbTransactionManager(@Qualifier("i18nDbEntityManagerFactory") LocalContainerEntityManagerFactoryBean secondDbEntityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(
                secondDbEntityManagerFactory.getObject()
        );

        return transactionManager;
    }

    @Bean
    public BrickTranslationAdminController brickTranslationController(TemplateService templateService, DefinitionService definitionService, BrickDefinitionConverter brickDefinitionConverter, TemplateConverter templateConverter) {
        return new BrickTranslationAdminController(templateService, definitionService, brickDefinitionConverter, templateConverter);
    }

    @Bean
    public BrickTranslationTranslatorController brickTranslationTranslatorController(TranslationService translationService, TemplateService templateService, BrickTranslationConverter brickTranslationConverter, TemplateConverter templateConverter) {
        return new BrickTranslationTranslatorController(translationService, templateService, brickTranslationConverter, templateConverter);
    }

    @Bean
    public BrickTranslationPublicController brickTranslationPublicController(TranslationDistService translationDistService) {
        return new BrickTranslationPublicController(translationDistService);
    }

    @Bean
    public TemplateService templateService(TemplateRepository templateRepository, @Lazy TranslationService translationService, TemplateFactory templateFactory, TemplateValidator templateValidator, TemplateConverter templateConverter) {
        return new TemplateServiceImpl(templateRepository, translationService, templateConverter, templateFactory, templateValidator);
    }

    @Bean
    public DefinitionService definitionService(BrickDefinitionRepository brickDefinitionRepository, BrickDefinitionConverter brickDefinitionConverter, BrickDefinitionFactory brickDefinitionFactory, BrickDefinitionValidator brickDefinitionValidator, TemplateService templateService) {
        return new DefinitionServiceImpl(brickDefinitionRepository, brickDefinitionConverter, brickDefinitionFactory, brickDefinitionValidator, templateService);
    }

    @Bean
    public TemplateConverter templateConverter() {
        return new TemplateConverter();
    }

    @Bean
    public TemplateValidator templateValidator() {
        return new TemplateValidator();
    }

    @Bean
    public BrickDefinitionValidator brickDefinitionValidator() {
        return new BrickDefinitionValidator();
    }

    @Bean
    public BrickDefinitionFactory brickDefinitionFactory() {
        return new BrickDefinitionFactory();
    }

    @Bean
    public TemplateFactory templateFactory() {
        return new TemplateFactory();
    }

    @Bean
    public BrickTranslationConverter brickTranslationConverter() {
        return new BrickTranslationConverter();
    }

    @Bean
    public BrickTranslationNextConverter brickTranslationNextConverter() {
        return new BrickTranslationNextConverter();
    }

    @Bean
    public BrickDefinitionConverter brickDefinitionConverter() {
        return new BrickDefinitionConverter();
    }

    @Bean
    public BrickTranslationNextValidator brickTranslationNextValidator() {
        return new BrickTranslationNextValidator();
    }

    @Bean
    public BrickTranslationNextFactory brickTranslationNextFactory() {
        return new BrickTranslationNextFactory();
    }

    @Bean
    public TranslationDistService translationDistService(TemplateService templateService, BrickTranslationRepository brickTranslationRepository, BrickTranslationNextRepository brickTranslationNextRepository, BrickTranslationConverter brickTranslationConverter, BrickTranslationNextConverter brickTranslationNextConverter) {
        return new TranslationDistServiceImpl(templateService, brickTranslationRepository, brickTranslationNextRepository, brickTranslationConverter, brickTranslationNextConverter);
    }

    @Bean
    public TranslationService translationService(TemplateService templateService, BrickTranslationNextFactory brickTranslationNextFactory, BrickTranslationNextValidator brickTranslationNextValidator, BrickTranslationRepository brickTranslationRepository, BrickTranslationNextRepository brickTranslationNextRepository, BrickDefinitionRepository brickDefinitionRepository, BrickDefinitionConverter brickDefinitionConverter, BrickTranslationConverter brickTranslationConverter, BrickTranslationNextConverter brickTranslationNextConverter) {
        return new TranslationServiceImpl(templateService, brickTranslationNextFactory, brickTranslationNextValidator, brickTranslationRepository, brickTranslationNextRepository, brickDefinitionRepository, brickDefinitionConverter, brickTranslationConverter, brickTranslationNextConverter);
    }
}
