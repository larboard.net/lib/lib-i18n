package net.larboard.lib.i18n.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public final class Enumeration {
    private Enumeration() {}

    public static boolean isValid(String value, Class<?> type) {
        if(value == null) {
            return false;
        }

        Field[] allFields = type.getDeclaredFields();
        for (Field field : allFields) {
            String s;
            try {
                s = (String)field.get(null);
            } catch (IllegalAccessException e) {
                throw new RuntimeException();
            }

            if(s == null) {
                throw new RuntimeException();
            }

            if(s.equals(value)) {
                return true;
            }
        }

        return false;
    }

    public static List<String> values(Class<?> type) {
        List<String> res = new ArrayList<>();

        Field[] allFields = type.getDeclaredFields();
        for (Field field : allFields) {
            String s;
            try {
                s = (String)field.get(null);
            } catch (IllegalAccessException e) {
                throw new RuntimeException();
            }

            if(s == null) {
                throw new RuntimeException();
            }

            res.add(s);
        }

        return res;
    }
}
