package net.larboard.lib.i18n.validator;

import net.larboard.lib.i18n.data.model.BrickDefinitionModel;
import net.larboard.lib.i18n.enumeration.BrickType;
import net.larboard.lib.i18n.exception.impl.ValidationException;
import net.larboard.lib.i18n.util.Enumeration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class BrickDefinitionValidator {
    public void validate(BrickDefinitionModel model) {
        List<ValidationException.ValidationError> errors = new ArrayList<>();

        if(model.getKey() == null || model.getKey().isBlank()) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "key",
                            "key must not be blank or null"
                    )
            );
        }

        if(model.getTemplate() == null || model.getTemplate() < 1) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "template",
                            "template id must not be null or less than 1"
                    )
            );
        }

        if(model.getMultiline() == null) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "multiline",
                            "multiline flag must nt be null"
                    )
            );
        }

        if(model.getDeleted() == null) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "deleted",
                            "deleted flag must nt be null"
                    )
            );
        }

        if(!Enumeration.isValid(model.getType(), BrickType.class)) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "type",
                            "invalid type"
                    )
            );
        }

        if(model.getPlaceholders() == null) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "placeholders",
                            "placeholders must be a list"
                    )
            );
        }

        if(!model.getPlaceholders().isEmpty()) {
            errors.addAll(
                    checkPlaceholdersForEmptyOrDuplicates(model)
            );
        }

        if(!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }

    private List<ValidationException.ValidationError> checkPlaceholdersForEmptyOrDuplicates(BrickDefinitionModel model) {
        List<ValidationException.ValidationError> errors = new ArrayList<>();

        Set<String> uniqueList = new HashSet<>();

        for(String placeholder : model.getPlaceholders()) {
            if(placeholder.isBlank()) {
                errors.add(
                        ValidationException.ValidationError.of(
                                "placeholder",
                                "placeholder must not be empty"
                        )
                );
            }

            if(uniqueList.contains(placeholder)) {
                errors.add(
                        ValidationException.ValidationError.of(
                                "placeholder:"+placeholder,
                                "duplicate placeholder"
                        )
                );
            }

            uniqueList.add(placeholder);
        }

        return errors;
    }
}
