package net.larboard.lib.i18n.validator;

import net.larboard.lib.i18n.data.model.BrickTranslationNextModel;
import net.larboard.lib.i18n.exception.impl.ValidationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BrickTranslationNextValidator {
    public void validate(BrickTranslationNextModel model) {
        List<ValidationException.ValidationError> errors = new ArrayList<>();

        if(model.getKey() == null || model.getKey().isBlank()) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "key",
                            "key must not be blank or null"
                    )
            );
        }

        if(model.getLang() == null || model.getLang().isBlank()) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "lang",
                            "lang must not be blank or null"
                    )
            );
        }

        if(model.getTemplate() == null || model.getTemplate() < 1) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "template",
                            "template must not be less than 1 or null"
                    )
            );
        }

        if(!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
