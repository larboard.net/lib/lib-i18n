package net.larboard.lib.i18n.validator;

import net.larboard.lib.i18n.data.model.TemplateModel;
import net.larboard.lib.i18n.enumeration.TemplateType;
import net.larboard.lib.i18n.exception.impl.ValidationException;
import net.larboard.lib.i18n.util.Enumeration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TemplateValidator {
    public void validate(TemplateModel model) {
        List<ValidationException.ValidationError> errors = new ArrayList<>();

        if(!Enumeration.isValid(model.getType(), TemplateType.class)) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "type",
                            "invalid type"
                    )
            );
        }

        if(model.getEnabledLanguages().isEmpty()) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "eanabledLanguages",
                            "must have at least one enabled language"
                    )
            );
        }

        if(!model.getEnabledLanguages().contains(model.getDefaultLanguage())) {
            errors.add(
                    ValidationException.ValidationError.of(
                            "defaultLanguage",
                            "default language must be in list of enabled languages"
                    )
            );
        }

        if(!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
