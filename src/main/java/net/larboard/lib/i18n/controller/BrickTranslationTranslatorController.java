package net.larboard.lib.i18n.controller;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.larboard.lib.i18n.converter.BrickTranslationConverter;
import net.larboard.lib.i18n.converter.TemplateConverter;
import net.larboard.lib.i18n.data.api.BrickAndTranslationRest;
import net.larboard.lib.i18n.data.api.BrickTranslationRest;
import net.larboard.lib.i18n.data.api.TemplateRest;
import net.larboard.lib.i18n.data.api.filter.BrickFilter;
import net.larboard.lib.i18n.data.api.filter.TemplateFilter;
import net.larboard.lib.i18n.data.api.patch.BrickTranslationUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickTranslationDto;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import net.larboard.lib.i18n.service.TemplateService;
import net.larboard.lib.i18n.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/i18n/translator")
@Tag(name = "i18n-translator")
@Api(tags = "i18n-translator")
public class BrickTranslationTranslatorController {
    private final TranslationService translationService;
    private final TemplateService templateService;
    private final BrickTranslationConverter brickTranslationConverter;
    private final TemplateConverter templateConverter;

    @Autowired
    public BrickTranslationTranslatorController(TranslationService translationService, TemplateService templateService, BrickTranslationConverter brickTranslationConverter, TemplateConverter templateConverter) {
        this.translationService = translationService;
        this.templateService = templateService;

        this.brickTranslationConverter = brickTranslationConverter;
        this.templateConverter = templateConverter;
    }

    @GetMapping("/template")
    public Page<TemplateRest> listTemplatesPagedAndFiltered(
            @RequestParam(name = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize,
            @Valid TemplateFilter filter
    ) {
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);

        Page<TemplateDto> dtos = templateService.listPagedAndFiltered(pageRequest, filter);

        return templateConverter.dtoToRest(dtos);
    }

    @GetMapping("/template/{templateId}")
    public TemplateRest getTemplate(@PathVariable Long templateId) {
        TemplateDto dto = templateService.getById(templateId);

        return templateConverter.dtoToRest(dto);
    }

    @GetMapping("/bricktranslation/{templateId}")
    public Page<BrickAndTranslationRest> listBrickTranslationsPagedAndFiltered(
            @PathVariable Long templateId,
            @RequestParam(name = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize,
            @Valid BrickFilter filter
    ) {

        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);

        return translationService.listBrickTranslationsPagedAndFiltered(pageRequest, templateId, filter);
    }

    @PatchMapping("/translation")
    public BrickTranslationRest saveTranslation(@RequestBody @Valid BrickTranslationUpdateRequest request) {
        BrickTranslationDto result = translationService.saveTranslation(request);

        return brickTranslationConverter.dtoToRest(result);
    }
}
