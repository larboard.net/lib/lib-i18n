package net.larboard.lib.i18n.controller;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.larboard.lib.i18n.converter.BrickDefinitionConverter;
import net.larboard.lib.i18n.converter.TemplateConverter;
import net.larboard.lib.i18n.data.BrickDefinitionId;
import net.larboard.lib.i18n.data.api.BrickDefinitionRest;
import net.larboard.lib.i18n.data.api.TemplateRest;
import net.larboard.lib.i18n.data.api.filter.BrickDefinitionFilter;
import net.larboard.lib.i18n.data.api.post.BrickDefinitionCreateRequest;
import net.larboard.lib.i18n.data.api.post.TemplateCreateRequest;
import net.larboard.lib.i18n.data.api.put.BrickDefinitionUpdateRequest;
import net.larboard.lib.i18n.data.api.put.TemplateUpdateRequest;
import net.larboard.lib.i18n.data.dto.BrickDefinitionDto;
import net.larboard.lib.i18n.data.dto.TemplateDto;
import net.larboard.lib.i18n.service.DefinitionService;
import net.larboard.lib.i18n.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/i18n/admin")
@Tag(name = "i18n-admin")
@Api(tags = "i18n-admin")
public class BrickTranslationAdminController {
    private final TemplateService templateService;
    private final DefinitionService definitionService;

    private final BrickDefinitionConverter brickDefinitionConverter;
    private final TemplateConverter templateConverter;

    @Autowired
    public BrickTranslationAdminController(TemplateService templateService, DefinitionService definitionService, BrickDefinitionConverter brickDefinitionConverter, TemplateConverter templateConverter) {
        this.templateService = templateService;
        this.definitionService = definitionService;

        this.brickDefinitionConverter = brickDefinitionConverter;
        this.templateConverter = templateConverter;
    }

    @PostMapping("/template")
    public TemplateRest createTemplate(@RequestBody @Valid TemplateCreateRequest request) {
        TemplateDto dto = templateService.create(request);

        return templateConverter.dtoToRest(dto);
    }

    @PutMapping("/template/{templateId}")
    public TemplateRest updateTemplate(@PathVariable Long templateId, @RequestBody @Valid TemplateUpdateRequest request) {
        TemplateDto dto = templateService.update(templateId, request);

        return templateConverter.dtoToRest(dto);
    }

    @DeleteMapping("/template/{templateId}")
    public void deleteTemplate(@PathVariable Long templateId) {
        templateService.delete(templateId);
    }

    @PutMapping("/template/{templateId}/release")
    public void releaseTranslations(@PathVariable Long templateId) {
        templateService.release(templateId);
    }

    @PostMapping("/brick")
    public BrickDefinitionRest saveNewBrick(@RequestBody @Valid BrickDefinitionCreateRequest request) {
        BrickDefinitionDto result = definitionService.createBrickDefinition(request);

        return brickDefinitionConverter.dtoToRest(result);
    }

    @PutMapping("/brick/{templateId}/{brickKey}")
    public BrickDefinitionRest updateBrick(
            @PathVariable Long templateId,
            @PathVariable String brickKey,
            @RequestBody @Valid BrickDefinitionUpdateRequest request
    ) {

        BrickDefinitionDto result = definitionService.updateBrickDefinition(
                BrickDefinitionId.of(
                        brickKey,
                        templateId
                ),
                request
        );

        return brickDefinitionConverter.dtoToRest(result);
    }

    @GetMapping("/brick/{templateId}/{brickKey}")
    public BrickDefinitionRest updateBrick(
            @PathVariable Long templateId,
            @PathVariable String brickKey
    ) {

        BrickDefinitionDto result = definitionService.getById(
                BrickDefinitionId.of(
                        brickKey,
                        templateId
                )
        );

        return brickDefinitionConverter.dtoToRest(result);
    }

    @DeleteMapping("/brick/{templateId}/{brickKey}")
    public void deleteBrick(@PathVariable Long templateId, @PathVariable String brickKey) {
        definitionService.deleteBrickDefinition(
                BrickDefinitionId.of(
                        brickKey,
                        templateId
                )
        );
    }

    @GetMapping("/brick/{templateId}")
    public Page<BrickDefinitionRest> listBricksPagedAndFiltered(
            @PathVariable Long templateId,
            @RequestParam(name = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize,
            @Valid BrickDefinitionFilter filter
    ) {

        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);

        Page<BrickDefinitionDto> result = definitionService.listBricksPagedAndFiltered(templateId, pageRequest, filter);

        return brickDefinitionConverter.dtoToRest(result);
    }
}
