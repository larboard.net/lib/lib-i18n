package net.larboard.lib.i18n.controller;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.larboard.lib.i18n.enumeration.DistFormat;
import net.larboard.lib.i18n.service.TranslationDistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/i18n/public")
@Tag(name = "i18n-public")
@Api(tags = "i18n-public")
public class BrickTranslationPublicController {
    private final TranslationDistService translationDistService;

    @Autowired
    public BrickTranslationPublicController(TranslationDistService translationDistService) {
        this.translationDistService = translationDistService;
    }

    @GetMapping("/dist/{template}/{language}")
    public Object getCurrentRelease(@PathVariable String template,
                                    @PathVariable String language,
                                    @RequestParam(name = "format", required = false, defaultValue = DistFormat.JSON) String format) {
        return translationDistService.getDist(template, language, format);
    }

    @GetMapping("/next/{template}/{language}")
    public Object getNextRelease(@PathVariable String template,
                                 @PathVariable String language,
                                 @RequestParam(name = "format", required = false, defaultValue = DistFormat.JSON) String format) {
        return translationDistService.getNext(template, language, format);
    }
}